# Extraction of circles from Logan assemblies

## Running
 
    acc=DRR001190
    zstd -d $acc.contigs.fa.zst

    python3 circles.py $acc.contigs.fa 31 $acc.contigs.circles.fa


## Legacy code


With graph-tool, but needs to be further modified for undirected cycle detection.

    apptainer pull docker://tiagopeixoto/graph-tool

    # test graph-tool installation with:    
    apptainer exec graph-tool_latest.sif python3 -c "import graph_tool.all"

    acc=DRR001190
    zstd -d $acc.contigs.fa.zst

    apptainer exec --bind $PWD:/mnt graph-tool_latest.sif /bin/bash -c "cd /mnt && ./time python3 /mnt/old/circles.graphtool.py $acc.contigs.fa 31 $acc.contigs.circles.fa"

