# see pyproject.toml
__version__ = "0.1.2"
__author__ = "Saito Tsutomu <tsutomu7@hotmail.co.jp>"
# some modifications by R Chikhi

import numpy as np

class unionfind:
    def __init__(self, n: int) -> None:
        self.parent = np.zeros((n,), dtype=np.uint64)
        for i in range(len(self.parent)):
            self.parent[i] = i

    def find(self, i: int) -> int:
        if self.parent[i] != i:
            self.parent[i] = self.find(self.parent[i])
        return self.parent[i]

    def unite(self, i: int, j: int) -> None:
        i = self.find(i)
        j = self.find(j)
        if i != j:
            self.parent[i] = j

    def issame(self, i: int, j: int) -> bool:
        return self.find(i) == self.find(j)

    def all_ccs(self):
        ccs = {}
        for i in range(len(self.parent)):
            p = self.find(i)
            if p not in ccs:
                ccs[p] = 0
            ccs[p] += 1
        return ccs

    # return only small ccs
    def small_groups(self, ccs, threshold) -> list[list[int]]:
        ccs2 = {}
        for i in range(len(self.parent)):
            p = self.find(i)
            if ccs[p] <= threshold:
                if p not in ccs2:
                    ccs2[p] = []
                ccs2[p] += [i]
        return ccs2.items()

