#!/usr/bin/env python3

import sys
import csv
from collections import defaultdict
from scipy.stats import fisher_exact

def main():
    """
    Example usage:
    python clustering_enrichment.py metaT.T.txt orfs.linclust_cluster.tsv total_metaT_count total_other_count sra_taxid.csv.zst
    
    where:
      - metaT.T.txt: one accession per line, belonging to transcriptomes.
      - orfs.linclust_cluster.tsv: two columns: centroid <tab> member_seq
      - total_metaT_count: number of metaT accessions overall (e.g., 5 million).
      - total_other_count: number of other (genome) accessions overall.
      - sra_taxid.csv.zst: A compressed CSV file with SRA accessions and taxonomy IDs.
    """
    if len(sys.argv) < 6:
        print("Usage: python clustering_enrichment.py metaT.T.txt orfs.linclust_cluster.tsv total_metaT_count total_other_count sra_taxid.csv.zst")
        sys.exit(1)

    metaT_file = sys.argv[1]
    clusters_file = sys.argv[2]
    total_metaT_count = int(sys.argv[3])
    total_other_count = int(sys.argv[4])
    taxid_file = sys.argv[5]

    # 1. Read metaT accessions into a set
    metaT_accessions = set()
    with open(metaT_file, 'r') as f:
        for line in f:
            acc = line.strip()
            if acc:
                metaT_accessions.add(acc)
    sys.stderr.write("read all T/metaT accessions\n")

    # 2. Read taxonomy data and identify human accessions (taxid 9606)
    import subprocess
    import io
    
    human_accessions = set()
    
    # Use zstdcat to decompress the file and read it
    cmd = ["zstdcat", taxid_file]
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    output, error = process.communicate()
    
    if process.returncode != 0:
        sys.stderr.write(f"Error decompressing {taxid_file}: {error}\n")
        sys.exit(1)
    
    # Parse the CSV data
    csv_reader = csv.reader(io.StringIO(output))
    for row in csv_reader:
        if len(row) >= 4 and row[3] == "9606":  # Human taxid
            accession = row[0].strip('"')
            human_accessions.add(accession)
    
    sys.stderr.write(f"read all human accessions (count: {len(human_accessions)})\n")

    # 3. Parse clusters and count membership
    #    Keep track of how many from metaT vs. not metaT and how many human for each centroid
    centroid_counts = defaultdict(lambda: {"metaT": 0, "other": 0, "human": 0, "members": set()})

    with open(clusters_file, 'r') as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            centroid, member = line.split('\t')
            member_acc = member.split("_")[0]
            centroid_counts[centroid]["members"].add(member_acc)
            
            if member_acc in metaT_accessions:
                centroid_counts[centroid]["metaT"] += 1
            else:
                centroid_counts[centroid]["other"] += 1
                
            if member_acc in human_accessions:
                centroid_counts[centroid]["human"] += 1

    sys.stderr.write("read all centroids\n")

    # 4. Fisher's exact test for each centroid and calculate human percentage
    print("centroid\tmetaT_in_cluster\tother_in_cluster\tfraction_metaT\tp_value\thuman_score")
    for centroid, counts in centroid_counts.items():
        in_metaT = counts["metaT"]
        in_other = counts["other"]
        human_count = counts["human"]
        total_members = len(counts["members"])
        
        if total_members < 5: continue
        
        out_metaT = total_metaT_count - in_metaT
        out_other = total_other_count - in_other

        # Build 2x2 contingency table
        contingency = [[in_metaT, in_other],
                       [out_metaT, out_other]]
        oddsratio, p_value = fisher_exact(contingency, alternative='two-sided')

        fraction_metaT = 0.0
        if (in_metaT + in_other) > 0:
            fraction_metaT = in_metaT / (in_metaT + in_other)
            
        # Calculate human score (percentage of members that are human)
        human_score = 0.0
        if total_members > 0:
            human_score = human_count / total_members

        print(f"{centroid}\t{in_metaT}\t{in_other}\t{fraction_metaT:.4f}\t{p_value:.3e}\t{human_score:.4f}")


if __name__ == "__main__":
    main()
