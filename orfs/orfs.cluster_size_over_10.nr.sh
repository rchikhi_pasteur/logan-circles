diamond blastp \
    --db /local/databases/index/diamond/2.0/nr.dmnd \
    --query orfs.cluster_size_over_10.fasta \
    --out orfs.cluster_size_over_10.nr.tsv \
    -f 6 qseqid qstart qend qlen qstrand \
             sseqid  sstart send slen \
             pident evalue cigar \
             full_qseq \
    --threads 50 \
    --evalue 1e-5 \
    --max-target-seqs 25 \
    --sensitive \
    --index-chunks 1 \
    --block-size 20.0

